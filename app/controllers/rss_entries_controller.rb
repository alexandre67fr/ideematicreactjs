class RssEntriesController < ApplicationController
  before_action :set_rss_entry, only: [:show, :edit, :update, :destroy]

  # GET /rss_entries
  # GET /rss_entries.json
  def index
    if params[:rss_feed_id]
      collection = RssFeed.find(params[:rss_feed_id]).rss_entries
    else
      collection = RssEntry.all
    end
    @rss_entries = collection.page(params[:page].to_i)
  end

  # GET /rss_entries/1
  # GET /rss_entries/1.json
  def show
  end

  # GET /rss_entries/new
  def new
    @rss_entry = RssEntry.new
  end

  # GET /rss_entries/1/edit
  def edit
  end

  # POST /rss_entries
  # POST /rss_entries.json
  def create
    @rss_entry = RssEntry.new(rss_entry_params)

    respond_to do |format|
      if @rss_entry.save
        format.html { redirect_to @rss_entry, notice: 'Rss entry was successfully created.' }
        format.json { render :show, status: :created, location: @rss_entry }
      else
        format.html { render :new }
        format.json { render json: @rss_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rss_entries/1
  # PATCH/PUT /rss_entries/1.json
  def update
    respond_to do |format|
      if @rss_entry.update(rss_entry_params)
        format.html { redirect_to @rss_entry, notice: 'Rss entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @rss_entry }
      else
        format.html { render :edit }
        format.json { render json: @rss_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rss_entries/1
  # DELETE /rss_entries/1.json
  def destroy
    @rss_entry.destroy
    respond_to do |format|
      format.html { redirect_to rss_entries_url, notice: 'Rss entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rss_entry
      @rss_entry = RssEntry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rss_entry_params
      params.require(:rss_entry).permit(:is_read)
    end
end
