class RssFeed < ApplicationRecord
	has_many :rss_entries, dependent: :destroy

	default_scope { order(created_at: :desc) }

	after_create do |feed|
		RssFeedsHelper.update_feed feed
	end

	def title
		begin
			return URI.parse(url).host if super.empty?
		rescue
		end
		super
	end
end
