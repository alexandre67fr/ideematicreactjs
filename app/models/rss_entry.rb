class RssEntry < ApplicationRecord
  belongs_to :rss_feed
  paginates_per 5
  default_scope { order(date: :desc) }
end
