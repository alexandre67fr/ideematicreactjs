require 'htmlentities'
json.extract! rss_entry, :id, :created_at, :updated_at, :id, :title, :url, :is_read
json.content HTMLEntities.new.decode truncate(strip_tags(rss_entry.content), length: 300)
json.date rss_entry.date.strftime("%d/%m/%Y") unless rss_entry.date.nil?
