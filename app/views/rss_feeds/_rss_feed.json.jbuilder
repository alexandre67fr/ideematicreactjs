json.extract! rss_feed, :title, :url, :id
json.pages rss_feed.rss_entries.page(0).total_pages
