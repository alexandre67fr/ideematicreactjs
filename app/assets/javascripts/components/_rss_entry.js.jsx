class RssEntry extends React.Component {
	constructor(props) {
	  super(props);
	  this.state = {
	    rss_entry: props.rss_entry
	  };
	}
	componentDidMount() {
	}
	changeReadState() {
		this.state.rss_entry.is_read = this.state.rss_entry.is_read ? 0 : 1;
		let body = JSON.stringify({ rss_entry: { is_read: parseInt(this.state.rss_entry.is_read) } });
		this.setState({rss_entry: this.state.rss_entry});
		fetch("/rss_entries/" + this.state.rss_entry.id + ".json", {
		  method: "PATCH",
		  headers: {
		    "Content-Type": "application/json"
		  },
		  body: body
		})
		  .then(response => {
		    return response.json();
		  })
		  .then(rss_feed => {
		  });
	}
	render() {
		let read_state;
		if (this.state.rss_entry.is_read) {
			read_state = <span>✓ Lu</span>
		}
		else {
			read_state = <a href="#" onClick={ (e) => { this.changeReadState(); e.preventDefault(); } }>Marquer comme lu</a>
		}
		
	    return(
		    <div key={this.state.rss_entry.id}>
		      <div className="d-flex w-100 justify-content-between">
		        <h5 className="mb-1">
		        	<a href={this.state.rss_entry.url} target="_blank">{this.state.rss_entry.title}</a>
		        </h5>
		        <small className="text-muted text-nowrap mt-1 pl-1">{this.state.rss_entry.date}</small>
		      </div>
		      <p className="mb-1">{this.state.rss_entry.content}</p>
		      <div className="text-right">
		      	<small className="text-muted">
		      	  {read_state}
		      	</small>
		      </div>
		    </div>
	    );
	}
}