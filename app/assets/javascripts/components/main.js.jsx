class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rss_feeds: [],
      showForm: false
    };
    this.handleAddFeed = this.handleAddFeed.bind(this);
    this.handleDeleteFeed = this.handleDeleteFeed.bind(this)
    this.addNewRssFeed = this.addNewRssFeed.bind(this);
    this.toggleForm = this.toggleForm.bind(this);
  }
  handleDeleteFeed(id){
    fetch("/rss_feeds/" + id + ".json", 
    {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then((response) => { 
    });
    var state = this.state;
    state.rss_feeds = this.state.rss_feeds.filter((feed) => feed.id != id)
    this.setState(state);
  }
  handleAddFeed(url, title) {
    let body = JSON.stringify({ rss_feed: { url: url, title: title } });
    fetch("/rss_feeds.json", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: body
    })
      .then(response => {
        return response.json();
      })
      .then(rss_feed => {
        this.addNewRssFeed(rss_feed);
      });
  }
  addNewRssFeed(rss_feed) {
    var state = this.state;
    state.rss_feeds = [rss_feed].concat(this.state.rss_feeds);
    this.setState(state);
  }
  toggleForm() {
    var state = this.state;
    state.showForm = !state.showForm;
    this.setState(state);
  }
  componentDidMount() {
    fetch("/rss_feeds.json")
      .then(response => {
        return response.json();
      })
      .then(data => {
        this.setState({ rss_feeds: data });
      });
  }
  render() {
    return (
      <div className="container pb-4 pt-4">
        { this.state.showForm ? <RssFeedForm toggleForm={this.toggleForm} handleAddFeed={this.handleAddFeed} /> : <RssFeedFormToggle toggleForm={this.toggleForm} /> }
        <RssFeeds rss_feeds={this.state.rss_feeds} handleDeleteFeed={this.handleDeleteFeed} />
      </div>
    );
  }
}
