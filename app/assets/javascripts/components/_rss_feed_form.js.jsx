const RssFeedForm = (props) => {
    let formFields = {}

    return(
      <form className='mb-4' onSubmit={ (e) => { props.handleAddFeed(formFields.url.value, formFields.title.value); e.target.reset(); props.toggleForm(); e.preventDefault(); }
}>
        <div className='form-group'>
            <input ref={input => formFields.title = input} className='form-control' placeholder='Titre du flux'/>
        </div>
        <div className='form-group'>
            <input type='url' required='required' ref={input => formFields.url = input}  className='form-control' placeholder='URL du flux' />
        </div>
        <div className='text-right'>
            <button className='btn btn-primary'>Ajouter</button>
            <button className='btn btn-secondary ml-2' onClick={ (e) => { props.toggleForm(); } }>Annuler</button>
        </div>
      </form>
    )

}