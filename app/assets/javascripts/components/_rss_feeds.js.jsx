const RssFeeds = (props) => {
	var rss_feeds = props.rss_feeds.map((rss_feed) => {
	    return(
	    	<div key={"rss-feed-" + rss_feed.id} className='col-md-6 rss-feed'>
        		<RssFeed rss_feed={rss_feed} handleDeleteFeed={props.handleDeleteFeed} />
    		</div>
	    )
    })
	return(
	      <div className='row rss-feeds'>
	        {rss_feeds}
	      </div>
	    )
	}
