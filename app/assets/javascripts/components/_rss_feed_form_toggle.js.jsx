const RssFeedFormToggle = (props) => {
    return(
      <div className='mb-4 text-right'>
        <button className='btn btn-primary' onClick={ (e) => { props.toggleForm(); } }>Ajouter un flux</button>
      </div>
    )
}