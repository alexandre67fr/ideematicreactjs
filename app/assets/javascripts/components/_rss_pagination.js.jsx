const RssPagination = (props) => {
	if (props.rss_feed.pages <= 1) return null;
	var pages = [];
	for (let page=1; page<=props.rss_feed.pages; page++) {
		pages.push (
	    	<li className={ (page == props.rss_feed.page) ? "page-item active" : "page-item" } key={"rss-feed-page-" + page}>
	    		<a href="#" className='page-link' onClick={ (e) => { props.loadPage(page); e.preventDefault(); }}>
	        		{page}
	        	</a>
	        </li>
	    );
	}
    return(
   	  <nav>
		  <ul className="pagination mb-0">
		  	{pages}
		  </ul>
	  </nav>
    )
}