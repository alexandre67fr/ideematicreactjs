const RssEntries = (props) => {
	var rss_entries = props.rss_entries.map((rss_entry) => {
	    return(
	    	<div key={"rss-entry-" + rss_entry.id} className="list-group-item flex-column align-items-start rss-entry">
	        	<RssEntry rss_entry={rss_entry}/>
	        </div>
	    )
    })
    return rss_entries;
}