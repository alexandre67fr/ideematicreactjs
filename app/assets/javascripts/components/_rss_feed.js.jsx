class RssFeed extends React.Component {
	constructor(props) {
	  super(props);
	  this.state = {
	  	rss_feed: props.rss_feed,
	    rss_entries: []
	  };
      this.loadPage = this.loadPage.bind(this);
	}
	componentDidMount() {
	  this.loadPage(1);
	}
	loadPage(page) {
	  this.state.rss_feed.page = page;
	  fetch("/rss_feeds/" + this.state.rss_feed.id + "/rss_entries.json?page=" + page)
	    .then(response => {
	      return response.json();
	    })
	    .then(data => {
	      this.setState({ rss_entries: data });
	    });
	}
	render() {
	    return(
	    	<div className="card mb-4 shadow-sm">
	    	  <div className="card-body">
	    	    <h4 className="card-title mb-0">
	    	    	<a href="#" className="close pull-right" onClick={() => this.props.handleDeleteFeed(this.state.rss_feed.id)}>&times;</a>
	    	    	Les news de {this.state.rss_feed.title}
	    	    </h4>
	    	  </div>
	  		  <div className="list-group list-group-flush">
    	  	    <RssEntries rss_feed={this.state.rss_feed} rss_entries={this.state.rss_entries} />
    	  	  	<RssPagination rss_feed={this.state.rss_feed} loadPage={this.loadPage} />
    	  	  </div>
	    	</div>
	    );
	}
}