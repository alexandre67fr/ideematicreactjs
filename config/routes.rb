Rails.application.routes.draw do
  resources :rss_feeds do
  	collection do 
  		get :refresh
  	end
  	resources :rss_entries
  end
  resources :rss_entries
  root 'home#index'
end
