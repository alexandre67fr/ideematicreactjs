# Installation

```
git clone https://bitbucket.org/alexandre67fr/ideematicreactjs.git ideematic-rss-reader
cd ideematic-rss-reader
bundle
```

Update MySQL login, password and database in `config/database.yml`.

```
rails db:migrate
whenever --update-crontab
rails s
```

# TODO

- RSpec tests for `RssFeedsHelper`
- rename `date` column to `published_at` (RoR naming convensions)
- loading indicator (spinner)
- cleanup unnecessary files
- API documentation

# Screenshots

![1](https://bytebucket.org/alexandre67fr/ideematic-jquery/raw/231fd2400a298d97377853b57341c52b85a235fb/Screenshots/Add%20a%20Feed.png)
![2](https://bytebucket.org/alexandre67fr/ideematic-jquery/raw/231fd2400a298d97377853b57341c52b85a235fb/Screenshots/Rss%20Feeds.png)
![3](https://bytebucket.org/alexandre67fr/ideematic-jquery/raw/231fd2400a298d97377853b57341c52b85a235fb/Screenshots/Rails%20version.png)

