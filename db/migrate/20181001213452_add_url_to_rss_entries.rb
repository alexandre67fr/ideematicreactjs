class AddUrlToRssEntries < ActiveRecord::Migration[5.2]
  def change
    add_column :rss_entries, :url, :string
  end
end
